use super::prelude::*;

use expressions::Variable;

#[derive(Debug, Clone)]
pub struct Function(
    pub ExpressionType,
    pub &'static str,
    pub Vec<Variable>,
    pub Body,
);
