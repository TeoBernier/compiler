use super::prelude::*;

#[derive(Debug, Clone)]
pub struct Body(pub Vec<Statement>);
