#[derive(Debug, Clone, Copy, PartialEq)]
pub enum ExpressionType {
    Void(usize),
    Int(usize),
    Float(usize),
    Custom(&'static str, usize),
    Unknown,
}

impl ExpressionType {
    pub fn address_of(self) -> Self {
        use ExpressionType::*;
        match self {
            Int(base) => Int(base + 1),
            Float(base) => Float(base + 1),
            Custom(name, base) => Custom(name, base + 1),
            _ => unreachable!(),
        }
    }

    pub fn deref(self) -> Self {
        use ExpressionType::*;
        match self {
            Int(base) => Int(base
                .checked_sub(1)
                .expect("Cannot dereference a non-pointer type")),
            Float(base) => Float(
                base.checked_sub(1)
                    .expect("Cannot dereference a non-pointer type"),
            ),
            Custom(name, base) => Custom(
                name,
                base.checked_sub(1)
                    .expect("Cannot dereference a non-pointer type"),
            ),
            _ => unreachable!(),
        }
    }

    pub fn checked_add(self, other: Self) -> Self {
        use ExpressionType::*;
        let expr_type = self + other;
        if expr_type == Unknown {
            panic!("Cannot combined types '{:?}' and '{:?}' !", self, other)
        } else {
            expr_type
        }
    }

    pub fn modify_deepness(self, deepness: isize) -> Self {
        use ExpressionType::*;
        match self {
            Int(base) => Int((base as isize)
                .checked_add(deepness)
                .expect("Cannot dereference a non-pointer type")
                as usize),
            Float(base) => Float(
                (base as isize)
                    .checked_add(deepness)
                    .expect("Cannot dereference a non-pointer type") as usize,
            ),
            Custom(name, base) => Custom(
                name,
                (base as isize)
                    .checked_add(deepness)
                    .expect("Cannot dereference a non-pointer type") as usize,
            ),
            _ => unreachable!(),
        }
    }
}

use std::ops::Add;

impl Add for ExpressionType {
    type Output = Self;
    fn add(self, other: ExpressionType) -> Self::Output {
        use ExpressionType::*;
        if self == other {
            other
        } else {
            match (self, other) {
                (Int(a), Float(b)) if a == b => Float(a),
                (Float(a), Int(b)) if a == b => Float(a),
                (Int(0), Int(a))
                | (Int(0), Float(a))
                | (Int(0), Custom(_, a))
                | (Int(0), Void(a))
                    if a > 0 =>
                {
                    other
                }
                (Int(a), Int(0))
                | (Float(a), Int(0))
                | (Custom(_, a), Int(0))
                | (Void(a), Int(0))
                    if a > 0 =>
                {
                    self
                }

                _ => Unknown,
            }
        }
    }
}
