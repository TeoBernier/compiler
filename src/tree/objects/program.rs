use super::prelude::*;

#[derive(Debug, Clone)]
pub struct Program(pub Vec<Function>);
