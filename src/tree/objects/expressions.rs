use super::expression_type::ExpressionType;
use crate::token::token_type::Token;

#[derive(Debug, Clone, Copy)]
pub struct Variable(pub &'static str, pub ExpressionType);

#[derive(Debug, Clone)]
pub enum Expression {
    ConstantInt(u64),
    ConstantFloat(f64),
    UnaryOperation(Token, Box<Expression>),
    BinaryOperation(Token, Box<Expression>, Box<Expression>),
    ExpressionVariable(Variable),
    Cast(ExpressionType, Box<Expression>),
    Assignment(Token, Box<Expression>, Box<Expression>),
    Comma(Box<Expression>, Box<Expression>),
    VoidExpression,
}

impl Expression {
    pub fn is_attributable(&self) -> bool {
        use Expression::*;
        match self {
            ExpressionVariable(..) => true,
            UnaryOperation(Token::Multiplication, _) => true,
            _ => false,
        }
    }

    pub fn extract_variable_pointers(&self) -> (&'static str, usize) {
        use Expression::*;
        use Token::*;
        match self {
            ExpressionVariable(Variable(name, ..)) => (name, 0),
            Expression::Assignment(_, expr, _) => expr.extract_variable_pointers(),
            UnaryOperation(Multiplication, expr) => {
                let (name, deepness) = expr.extract_variable_pointers();
                (name, deepness + 1)
            }
            _ => unreachable!("Cannot exctract variable from {:?}", self),
        }
    }

    pub fn get_type(&self) -> ExpressionType {
        use Expression::*;
        use ExpressionType::*;
        match self {
            ConstantInt(_) => Int(0),
            VoidExpression => Void(0),
            ConstantFloat(_) => Float(0),
            UnaryOperation(Token::BitwiseAnd, expr) => expr.get_type().address_of(),
            UnaryOperation(Token::Multiplication, expr) => expr.get_type().deref(),
            BinaryOperation(_, expr1, expr2) => expr1.get_type().checked_add(expr2.get_type()),
            &ExpressionVariable(Variable(_, expr_type)) | &Cast(expr_type, _) => expr_type,
            UnaryOperation(_, expr) | Assignment(_, expr, _) | Comma(_, expr) => expr.get_type(),
        }
    }
}
