use super::prelude::*;

#[derive(Debug, Clone)]
pub struct Declaration(pub &'static str, pub Option<Expression>);

#[derive(Debug, Clone)]
pub enum Statement {
    Return(Expression),
    Declare(Declaration),
    DeclarationList(Vec<Declaration>),
    ExpressionStatement(Expression),
    Else(Box<Statement>),
    If(Expression, Box<Statement>, Option<Box<Statement>>),
    While(Expression, Box<Statement>),
    BodyStatement(Body),
    For(Box<Statement>, Expression, Expression, Box<Statement>),
}

impl Statement {
    pub fn try_add_else_statement(
        &mut self,
        else_statement: Box<Statement>,
    ) -> Result<(), Box<Statement>> {
        use Statement::*;
        match self {
            For(_, _, _, expr) | While(_, expr) => expr.try_add_else_statement(else_statement),
            If(_, if_expr, ref mut some_else_statement) => {
                match some_else_statement {
                    Some(else_expr) => else_expr.try_add_else_statement(else_statement),
                    None => match if_expr.try_add_else_statement(else_statement) {
                        Ok(()) => Ok(()),
                        Err(else_statement) => {
                            some_else_statement.get_or_insert(else_statement);
                            Ok(())
                        }
                    },
                }
            }
            _ => Err(else_statement),
        }
    }
}
