use super::objects::prelude::*;
use std::sync::atomic::{AtomicU32, Ordering};

use ExpressionType::*;

static COUNT: AtomicU32 = AtomicU32::new(0);

use lazy_static::lazy_static;
use std::collections::HashMap;
use std::ops::DerefMut;
use std::sync::Mutex;

lazy_static! {
    #[derive(Debug)]
    static ref LOCAL_VARIABLES: Mutex<HashMap<&'static str, (u32, u32)>> = Mutex::new(HashMap::new());
}

static VARIABLE_OFFSET: AtomicU32 = AtomicU32::new(8);

static SCOPE_DEEPNESS: AtomicU32 = AtomicU32::new(0);

use expressions::Variable;
use statements::Declaration;

pub fn generate_assignment(expression: &Expression) -> String {
    use super::super::token::token_type::Token;
    use Expression::*;
    use Token::*;
    match expression {
        ExpressionVariable(Variable(name, Int(_))) => format!(
            "\tmov \t%rax,    \t{}(%rbp)\n",
            -(LOCAL_VARIABLES
                .lock()
                .as_ref()
                .unwrap()
                .get(name)
                .expect("Cannot use variables before declaration")
                .0 as i32)
        ),

        UnaryOperation(Multiplication, expr) => {
            "\tpush \t%rax\n".to_string()
                + &generate_expression(expr)
                + "\tpop \t%rcx\n\tmov \t%rcx,     \t(%rax)\n\tmov \t%rcx,     \t%rax\n"
        }

        _ => unreachable!("Souldn't be reached : {:?}", expression),
    }
}

pub fn generate_expression(expression: &Expression) -> String {
    use super::super::token::token_type::{assignment_to_binary_operation, Token};
    use Expression::*;
    use Token::*;
    match expression {
        ExpressionVariable(Variable(name, Int(_))) => format!(
            "\tmov \t{}(%rbp), \t%rax\n",
            -(LOCAL_VARIABLES
                .lock()
                .as_ref()
                .unwrap()
                .get(name)
                .expect("Cannot use variables before declaration")
                .0 as i32)
        ),

        Expression::Assignment(op, expression_dest, expression_source) => match op {
            Token::Assignment => {
                generate_expression(expression_source) + &generate_assignment(expression_dest)
            }
            token => generate_expression(&Expression::Assignment(
                Token::Assignment,
                expression_dest.clone(),
                Box::new(BinaryOperation(
                    assignment_to_binary_operation(token),
                    expression_dest.clone(),
                    expression_source.clone(),
                )),
            )),
        },

        ConstantInt(number) => {
            "\tmov \t".to_string() + "$" + &number.to_string() + ",     \t%rax\n"
        }

        UnaryOperation(BitwiseAnd, expr) => match expr.as_ref() {
            ExpressionVariable(Variable(name, Int(_))) => {
                format!(
                    "\tmov \t%rbp,     \t%rax\n\tadd \t${},    \t%rax\n",
                    -(LOCAL_VARIABLES
                        .lock()
                        .as_ref()
                        .unwrap()
                        .get(name)
                        .expect("Cannot use variables before declaration")
                        .0 as i32)
                )
            }
            _ => panic!(
                "Cannot get address of an expression which is not a variable ! {:?}",
                expr
            ),
        },

        UnaryOperation(Multiplication, expr) => {
            generate_expression(expr) + "\tmov \t(%rax),  \t%rax\n"
        }

        UnaryOperation(token, expression) => {
            generate_expression(expression)
                + match token {
                    Negation => "\tneg \t%rax\n",
                    BitwiseComplement => "\tnot \t%rax\n",
                    LogicalNegation => {
                        "\tcmp \t$0,     \t%rax\n\tmov \t$0,     \t%rax\n\tsete \t%al\n"
                    }
                    _ => panic!("Invalid Token for unary operation !"),
                }
        }

        BinaryOperation(op @ Or, expression1, expression2)
        | BinaryOperation(op @ And, expression1, expression2) => {
            let id = COUNT.load(Ordering::Relaxed);
            COUNT.store(COUNT.load(Ordering::Relaxed) + 1, Ordering::Relaxed);
            let assembly = generate_expression(expression1)
                + "\tcmp \t$0,     \t%rax\n\t"
                + if let Or = op {
                    "jne \t_end_"
                } else {
                    "je  \t_end_"
                }
                + &id.to_string()
                + "\n"
                + &generate_expression(expression2)
                + "\tcmp \t$0,     \t%rax\n_end_"
                + &id.to_string()
                + ":\n\tmov \t$0,     \t%rax\n\tsetne\t%al\n";
            assembly
        }

        BinaryOperation(token, expression1, expression2) => {
            generate_expression(expression1)
                + "\tpush \t%rax\n"
                + &generate_expression(expression2)
                + "\tmov \t%rax,     \t%rcx\n\tpop \t%rax\n"
                + match token {
                    Addition => "\tadd \t%rcx,     \t%rax\n",
                    Negation => "\tsub \t%rcx,     \t%rax\n",
                    Multiplication => "\timul \t%rcx,     \t%rax\n",
                    Division => "\tcdq\n\tidiv \t%ecx\n",
                    Remainder => "\tcdq\n\tidiv \t%ecx\n\tmov \t%edx,     \t%eax\n",
                    Equal => "\tcmp \t%rcx,     \t%rax\n\tmov \t$0,     \t%rax\n\tsete \t%al\n",
                    Greater => "\tcmp \t%rcx,     \t%rax\n\tmov \t$0,     \t%rax\n\tsetg \t%al\n",
                    Less => "\tcmp \t%rcx,     \t%rax\n\tmov \t$0,     \t%rax\n\tsetl \t%al\n",
                    GreaterEqual => "\tcmp \t%rcx, %rax\n\tmov \t$0, %rax\n\tsetge \t%al\n",
                    LessEqual => {
                        "\tcmp \t%rcx,     \t%rax\n\tmov \t$0,     \t%rax\n\tsetle \t%al\n"
                    }
                    BitwiseShiftLeft => "\tshl \t%cl,     \t%rax\n",
                    BitwiseShiftRight => "\tshr \t%cl,     \t%rax\n",
                    BitwiseAnd => "\tand \t%rcx,     \t%rax\n",
                    BitwiseOr => "\tor \t%rcx,     \t%rax\n",
                    BitwiseXor => "\txor \t%rcx,     \t%rax\n",
                    _ => panic!("Invalid Token for binary operation ! : {:?}", expression),
                }
        }

        Expression::Comma(expression1, expression2) => {
            generate_expression(expression1) + &generate_expression(expression2)
        }

        Expression::VoidExpression => "".into(),

        ConstantFloat(..) => unimplemented!("Not implemented for {:?}", expression),
        Cast(..) => unimplemented!("Not implemented for {:?}", expression),
        ExpressionVariable(..) => unimplemented!("Not implemented for {:?}", expression),
    }
}

pub fn generate_statement(statement: &Statement) -> String {
    use Statement::*;
    match statement {
        Return(expression) => {
            generate_expression(expression) + "\tmov \t%rbp,     \t%rsp\n\tpop \t%rbp\n\tret\n"
        }
        Declare(Declaration(name, expr)) => {
            if match LOCAL_VARIABLES.lock().unwrap().get(name) {
                Some(&(_, scope_deepness)) => {
                    scope_deepness == SCOPE_DEEPNESS.load(Ordering::Relaxed)
                }
                None => false,
            } {
                panic!("Redefinition of variable '{}' !", name);
            } else {
                LOCAL_VARIABLES.lock().unwrap().insert(
                    name,
                    (
                        VARIABLE_OFFSET.load(Ordering::Relaxed),
                        SCOPE_DEEPNESS.load(Ordering::Relaxed),
                    ),
                );
                VARIABLE_OFFSET.store(
                    VARIABLE_OFFSET.load(Ordering::Relaxed) + 8,
                    Ordering::Relaxed,
                );
                match expr {
                    Some(expression) => generate_expression(expression) + "\tpush \t%rax\n",
                    None => "\tpush \t$0\n".to_string(),
                }
            }
        }
        ExpressionStatement(expression) => generate_expression(expression),

        Statement::If(condition, if_statement, else_statement) => {
            let id = COUNT.load(Ordering::Relaxed);
            COUNT.store(COUNT.load(Ordering::Relaxed) + 2, Ordering::Relaxed);
            let mut s = generate_expression(condition)
                + "\tcmp \t$0,     \t%rax\n\tje  \t_end_"
                + &id.to_string()
                + "\n"
                + &generate_statement(if_statement);
            if let Some(statement) = else_statement {
                s += &("\tjmp \t_end_".to_string()
                    + &(id + 1).to_string()
                    + "\n_end_"
                    + &id.to_string()
                    + ":\n"
                    + &generate_statement(statement))
            }
            s + &"_end_".to_string() + &(id + 1).to_string() + ":\n"
        }

        Statement::While(condition, statement) => {
            let id = &COUNT.load(Ordering::Relaxed).to_string();
            COUNT.store(COUNT.load(Ordering::Relaxed) + 1, Ordering::Relaxed);
            "_begin_".to_string()
                + id
                + ":\n"
                + &generate_expression(condition)
                + "\tcmp \t$0,     \t%rax\n\tje  \t_end_"
                + id
                + "\n"
                + &generate_statement(statement)
                + "\tjmp \t_begin_"
                + id
                + "\n_end_"
                + id
                + ":\n"
        }

        Statement::For(init, cond, incr, statement) => {
            let copy = LOCAL_VARIABLES.lock().unwrap().clone();
            SCOPE_DEEPNESS.store(
                SCOPE_DEEPNESS.load(Ordering::Relaxed) + 1,
                Ordering::Relaxed,
            );

            let id = &COUNT.load(Ordering::Relaxed).to_string();
            COUNT.store(COUNT.load(Ordering::Relaxed) + 1, Ordering::Relaxed);
            let res = generate_statement(init)
                + "\tjmp \t_condition_"
                + id
                + "\n_begin_"
                + id
                + ":\n"
                + &generate_statement(statement)
                + &generate_expression(incr)
                + "_condition_"
                + id
                + ":\n"
                + &generate_expression(cond)
                + "\tcmp \t$0,     \t%rax\n\tjne  \t_begin_"
                + id
                + "\n";
            SCOPE_DEEPNESS.store(
                SCOPE_DEEPNESS.load(Ordering::Relaxed) - 1,
                Ordering::Relaxed,
            );

            *LOCAL_VARIABLES.lock().as_mut().unwrap().deref_mut() = copy;

            res
        }

        Statement::Else(_) => unreachable!(),

        Statement::DeclarationList(list) => list
            .iter()
            .map(|statement| generate_statement(&Declare(statement.clone())))
            .collect(),

        Statement::BodyStatement(body) => generate_body(body),
    }
}

pub fn generate_body(body: &Body) -> String {
    let copy = LOCAL_VARIABLES.lock().unwrap().clone();
    SCOPE_DEEPNESS.store(
        SCOPE_DEEPNESS.load(Ordering::Relaxed) + 1,
        Ordering::Relaxed,
    );

    let res = body
        .0
        .iter()
        .map(|statement| generate_statement(statement))
        .collect::<String>()
        + &format!(
            "\tadd \t${},     \t%rsp\n",
            (LOCAL_VARIABLES.lock().unwrap().len() - copy.len()) * 8
        );
    VARIABLE_OFFSET.store(
        VARIABLE_OFFSET.load(Ordering::Relaxed)
            - (LOCAL_VARIABLES.lock().unwrap().len() - copy.len()) as u32 * 8,
        Ordering::Relaxed,
    );
    SCOPE_DEEPNESS.store(
        SCOPE_DEEPNESS.load(Ordering::Relaxed) - 1,
        Ordering::Relaxed,
    );

    *LOCAL_VARIABLES.lock().as_mut().unwrap().deref_mut() = copy;

    res
}

pub fn generate_function(function: &Function) -> String {
    let copy = LOCAL_VARIABLES.lock().unwrap().clone();

    let res = match function {
        Function(_, name, _, body) => {
            "\t.globl\t".to_string()
                + &name
                + "\n"
                + &name
                + ":\n"
                + "\tpush \t%rbp\n\tmov \t%rsp,     \t%rbp\n"
                + &generate_body(body)
        }
    };
    VARIABLE_OFFSET.store(
        VARIABLE_OFFSET.load(Ordering::Relaxed)
            - (LOCAL_VARIABLES.lock().unwrap().len() - copy.len()) as u32 * 8,
        Ordering::Relaxed,
    );
    *LOCAL_VARIABLES.lock().as_mut().unwrap().deref_mut() = copy;

    res
}

pub fn generate_program(program: &Program) -> String {
    *LOCAL_VARIABLES.lock().unwrap() = HashMap::<&'static str, (u32, u32)>::new();
    match program {
        Program(functions) => functions
            .iter()
            .map(|function| generate_function(function))
            .collect(),
    }
}
