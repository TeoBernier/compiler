use super::super::token::token_type::*;
use super::objects::prelude::*;
use Expression::*;
use ExpressionType::*;

use ParseError::*;
use ParseOk::*;
use ParseResult;
use Token::*;

use lazy_static::lazy_static;
use std::collections::HashMap;
use std::sync::Mutex;

use expressions::Variable;
use statements::Declaration;

lazy_static! {
    #[derive(Debug)]
    static ref LOCAL_VARIABLES_TYPES: Mutex<HashMap<&'static str, ExpressionType>> = Mutex::new(HashMap::new());
}

lazy_static! {
    #[derive(Debug)]

    pub static ref TYPES: Mutex<HashMap<&'static str, ExpressionType>> = Mutex::new({
        let mut hash_map = HashMap::new();
        hash_map.insert("void", Void(0));
        hash_map.insert("int", Int(0));
        hash_map.insert("float", Float(0));
        hash_map
    });
}

#[inline]
fn try_parse(tokens: &mut impl Iterator<Item = Token>) -> ParseResult {
    match tokens.next() {
        Some(token) => Ok(Parsed(token)),
        None => Err(EOF),
    }
}

fn parse_id(tokens: &mut impl Iterator<Item = Token>) -> (Option<Expression>, ParseResult) {
    match tokens.next() {
        // <int>
        Some(IntValue(number)) => (Some(ConstantInt(number)), try_parse(tokens)),

        // <id>
        Some(Identifier(name)) => {
            if let Some(&expr_type) = LOCAL_VARIABLES_TYPES.lock().unwrap().get(name) {
                (
                    Some(ExpressionVariable(Variable(name, expr_type))),
                    try_parse(tokens),
                )
            } else {
                (
                    Some(ExpressionVariable(Variable(name, Unknown))),
                    try_parse(tokens),
                )
            }
        }

        // "(" <expression> ")"
        Some(OpenParenthesis) => match parse_expression(tokens) {
            // "(" <expression> ")"
            (Some(expression), Ok(Parsed(CloseParenthesis))) => {
                (Some(expression), try_parse(tokens))
            }

            // "(" <expression>
            (Some(_), Ok(Parsed(token))) => (None, Err(Expected(CloseParenthesis, token))),

            // "(" <expression> (EOF | )
            (_, Err(err)) => (None, Err(err)),

            _ => (
                None,
                Err(Error("Error while parsing \"(\" <expression> \")\"")),
            ),
        },
        Some(token) => (None, Ok(Parsed(token))),

        None => (None, Err(EOF)),
    }
}

/*

    factor ::= "(" <expression> ")" | <int> | <id> [(++ | --)]
*/
fn parse_factor(tokens: &mut impl Iterator<Item = Token>) -> (Option<Expression>, ParseResult) {
    match parse_id(tokens) {
        // <id> => <id> (++ | -- | token | EOF)
        (Some(expression), Ok(Parsed(op @ Increment)))
        | (Some(expression), Ok(Parsed(op @ Decrement))) => match expression.is_attributable() {
            true => (
                Some(BinaryOperation(
                    Addition,
                    match op {
                        Increment => Box::new(UnaryOperation(Negation, Box::new(ConstantInt(1)))),
                        Decrement => Box::new(ConstantInt(1)),
                        _ => unreachable!(),
                    },
                    Box::new(Expression::Assignment(
                        AdditionAssignment,
                        Box::new(expression),
                        match op {
                            Increment => Box::new(ConstantInt(1)),
                            Decrement => {
                                Box::new(UnaryOperation(Negation, Box::new(ConstantInt(1))))
                            }
                            _ => unreachable!(),
                        },
                    )),
                )),
                try_parse(tokens),
            ),

            false => (
                None,
                Err(Error(
                    "Error while parsing <expr> ( ++ | -- ), <expr> cannot be assigned a value",
                )),
            ),
        },
        //we don't know, we just give it to the upper level
        parsed => parsed,
    }
}

/*

    unary_factor ::= [(- | ~ | !)] <factor> | (++ | --) <id> | <factor>
*/
pub fn parse_unary_factors(
    tokens: &mut impl Iterator<Item = Token>,
) -> (Option<Expression>, ParseResult) {
    match parse_factor(tokens) {
        // [(- | ~ | !)]
        (None, Ok(Parsed(op @ Negation)))
        | (None, Ok(Parsed(op @ BitwiseComplement)))
        | (None, Ok(Parsed(op @ LogicalNegation)))
        | (None, Ok(Parsed(op @ BitwiseAnd)))
        | (None, Ok(Parsed(op @ Multiplication))) => {
            match parse_unary_factors(tokens) {
                // <factor>
                (Some(expression), result) => {
                    (Some(UnaryOperation(op, Box::new(expression))), result)
                }

                //
                (None, _) => (
                    None,
                    Err(Error("Error while parsing (- | ~ | ! | & | *) <factor>")),
                ),
            }
        }

        (None, Ok(Parsed(And))) => {
            match parse_unary_factors(tokens) {
                // <factor>
                (Some(expression), result) => (
                    Some(UnaryOperation(
                        BitwiseAnd,
                        Box::new(UnaryOperation(BitwiseAnd, Box::new(expression))),
                    )),
                    result,
                ),

                //
                (None, _) => (None, Err(Error("Error while parsing && <factor>"))),
            }
        }

        // ++ | --
        (None, Ok(Parsed(op @ Increment))) | (None, Ok(Parsed(op @ Decrement))) => {
            match parse_unary_factors(tokens) {
                (Some(expression), result) if expression.is_attributable() => (
                    Some(Expression::Assignment(
                        AdditionAssignment,
                        Box::new(expression),
                        match op {
                            Increment => Box::new(ConstantInt(1)),
                            Decrement => {
                                Box::new(UnaryOperation(Negation, Box::new(ConstantInt(1))))
                            }
                            _ => unreachable!(),
                        },
                    )),
                    result,
                ),

                _ => (
                    None,
                    Err(Error(
                        "Error while parsing ++ <expr>, expr missing or cannot be assigned a value",
                    )),
                ),
            }
        }

        parsed => parsed,
    }
}

macro_rules! parse_left_right_expression {
    ($func_name:ident, $func_call:ident, $($token:ident),+) => {
        pub fn $func_name (tokens: &mut impl Iterator<Item = Token>) -> (Option<Expression>, ParseResult) {
            let mut top_expression;
            let mut next_operator;
            match $func_call(tokens) {
                $( (Some(expression), Ok(Parsed(token @ Token::$token))) ) | + => {
                    top_expression = expression;
                    next_operator = token;
                }
                parsed => return parsed,
            }

            loop {
                match (next_operator, $func_call(tokens)) {
                    $( (op @ $token, (Some(expression), result)) ) | + => {
                        top_expression =
                            BinaryOperation(op, Box::new(top_expression), Box::new(expression));
                        match result {
                            $( Ok(Parsed(token @ $token)) ) | + => next_operator = token,
                            _ => return (Some(top_expression), result),
                        }
                    }
                    _ => unreachable!(),
                }
            }
        }
    }
}

parse_left_right_expression!(
    parse_term,
    parse_unary_factors,
    Multiplication,
    Division,
    Remainder
);
parse_left_right_expression!(parse_additive_expression, parse_term, Addition, Negation);
parse_left_right_expression!(
    parse_bitwise_shift_expression,
    parse_additive_expression,
    BitwiseShiftLeft,
    BitwiseShiftRight
);
parse_left_right_expression!(
    parse_relational_expression,
    parse_bitwise_shift_expression,
    Greater,
    Less,
    GreaterEqual,
    LessEqual
);
parse_left_right_expression!(
    parse_equality_expression,
    parse_relational_expression,
    Equal,
    NotEqual
);
parse_left_right_expression!(
    parse_bitwise_and_expression,
    parse_equality_expression,
    BitwiseAnd
);
parse_left_right_expression!(
    parse_bitwise_xor_expression,
    parse_bitwise_and_expression,
    BitwiseXor
);
parse_left_right_expression!(
    parse_bitwise_or_expression,
    parse_bitwise_xor_expression,
    BitwiseOr
);
parse_left_right_expression!(
    parse_logical_and_expression,
    parse_bitwise_or_expression,
    And
);
parse_left_right_expression!(
    parse_logical_or_expression,
    parse_logical_and_expression,
    Or
);

/*

    <assignment> ::= <id> { = <id> } = <expression>
*/
fn parse_assignment(tokens: &mut impl Iterator<Item = Token>) -> (Option<Expression>, ParseResult) {
    match parse_logical_or_expression(tokens) {
        (Some(expression_dest), Ok(Parsed(token))) if ASSIGNMENT_OPERATION.contains(&token) => {
            match expression_dest.is_attributable() {
                true => match parse_assignment(tokens) {
                    (Some(expression_src), result) => (
                        Some(Expression::Assignment(token, Box::new(expression_dest), Box::new(expression_src))),
                        result,
                    ),
                    _ => (None, Err(Error("Error while parsing <expr> = ..."))),
                },
                false => (None, Err(Error("Error while parsing <expr> = ... because <expr> cannot be assigned a value"))),
            }
        }
        parsed => return parsed,
    }
}

fn parse_comma(tokens: &mut impl Iterator<Item = Token>) -> (Option<Expression>, ParseResult) {
    match parse_assignment(tokens) {
        (Some(expression1), Ok(Parsed(Token::Comma))) => match parse_comma(tokens) {
            (Some(expression2), result) => (
                Some(Expression::Comma(
                    Box::new(expression1),
                    Box::new(expression2),
                )),
                result,
            ),

            (None, Err(err)) => return (None, Err(err)),

            (None, _) => (None, Err(Error("Error while parsing <expr>, <expr>"))),
        },
        parsed => return parsed,
    }
}

fn parse_expression(tokens: &mut impl Iterator<Item = Token>) -> (Option<Expression>, ParseResult) {
    parse_comma(tokens)
}

fn parse_declaration_statement(
    tokens: &mut impl Iterator<Item = Token>,
    mut declarations: Vec<Declaration>,
    declaration_type: ExpressionType,
) -> (Option<Statement>, ParseResult) {
    match parse_assignment(tokens) {
        (Some(expression), Ok(Parsed(op @ Token::Comma)))
        | (Some(expression), Ok(Parsed(op @ Semicolon))) => {
            let (name, deepness) = expression.extract_variable_pointers();

            let declaration_type_modify = declaration_type.modify_deepness(deepness as isize);

            LOCAL_VARIABLES_TYPES
                .lock()
                .unwrap()
                .insert(name, declaration_type_modify);
            declarations.push(Declaration(
                name,
                match expression {
                    Expression::Assignment(_, _, expr) => {
                        if declaration_type_modify != expr.get_type().checked_add(declaration_type_modify) {
                            panic!("ExpressionType error while parsing assignment {:?} : \n\ttype expected : {:?}\n\texpression type : {:?}\n\tfinal_type : {:?}", expr, declaration_type, expr.get_type(), expr.get_type().checked_add(declaration_type))
                        }
                        Some(expr.as_ref().clone())
                    }
                    _ => None,
                },
            ));
            match op {
                Token::Comma => parse_declaration_statement(tokens, declarations, declaration_type),
                Semicolon => (
                    Some(Statement::DeclarationList(declarations)),
                    Ok(NotParsed),
                ),
                _ => unreachable!(),
            }
        }
        _ => (
            None,
            Err(Error("Error while parsing <type> ... [, ...]* ;")),
        ),
    }
}

/*

    statement ::= "return" <expr> ";" | "int" <id> [= <int>] ";" | "int" <id> ";" | <expression> ";" | if "(" <expression> ")" ("{" <body "}" | <statement>) [ else ("{" <body "}" | <statement>) ]
*/
fn parse_basic_statement(
    tokens: &mut impl Iterator<Item = Token>,
) -> (Option<Statement>, ParseResult) {
    use Statement::*;
    match parse_expression(tokens) {
        // <expression> ";"
        (Some(expression), Ok(Parsed(Semicolon))) => {
            (Some(ExpressionStatement(expression)), Ok(NotParsed))
        }

        // ?
        (Some(_), Ok(Parsed(token))) => (None, Err(Expected(Semicolon, token))),

        // EOF
        (Some(_), Err(EOF)) => (None, Err(ExpectedButEOF(Semicolon))),

        (None, Ok(Parsed(Token::Return))) => match parse_expression(tokens) {
            // return <expression> ";"
            (Some(expression), Ok(Parsed(Semicolon))) => (Some(Return(expression)), Ok(NotParsed)),

            (None, Ok(Parsed(Semicolon))) => (Some(Return(VoidExpression)), Ok(NotParsed)),

            // ?
            (Some(_), Ok(Parsed(token))) => (None, Err(Expected(Semicolon, token))),

            // EOF
            (Some(_), Err(EOF)) => (None, Err(ExpectedButEOF(Semicolon))),
            _ => (
                None,
                Err(Error("Error while parsing return <expression> ;")),
            ),
        },

        (None, Ok(Parsed(OpenBrace))) => match parse_body(tokens) {
            (Some(body), Ok(NotParsed)) => (Some(BodyStatement(body)), Ok(NotParsed)),
            (None, Err(err)) => (None, Err(err)),
            _ => (
                None,
                Err(Error("Error while parsing body inside a statement")),
            ),
        },

        (None, Ok(Parsed(Semicolon))) => (Some(ExpressionStatement(VoidExpression)), Ok(NotParsed)),

        // int
        (None, Ok(Parsed(TypeToken(expr_type)))) => {
            parse_declaration_statement(tokens, Vec::new(), expr_type)
        }

        (None, parsed) => (None, parsed),
        (_, Err(err)) => (None, Err(err)),

        _ => unreachable!(),
    }
}

fn parse_if_else_statement(
    tokens: &mut impl Iterator<Item = Token>,
) -> (Option<Statement>, ParseResult) {
    match parse_basic_statement(tokens) {
        (None, Ok(Parsed(Token::If))) => match tokens.next() {
            Some(OpenParenthesis) => match parse_expression(tokens) {
                (Some(if_condition), Ok(Parsed(CloseParenthesis))) => match parse_statement(tokens)
                {
                    (Some(if_statement), Ok(NotParsed)) => (
                        Some(Statement::If(if_condition, Box::new(if_statement), None)),
                        Ok(NotParsed),
                    ),
                    (None, Err(err)) => (None, Err(err)),
                    _ => (
                        None,
                        Err(Error("Error while parsing if \"(\" <expression> \")\" ...")),
                    ),
                },
                (Some(_), Ok(Parsed(token))) => (None, Err(Expected(CloseParenthesis, token))),
                (Some(_), Err(EOF)) => (None, Err(ExpectedButEOF(CloseParenthesis))),
                _ => (
                    None,
                    Err(Error("Error while parsing if \"(\" <expression> \")\"")),
                ),
            },

            Some(token) => (None, Err(Expected(OpenParenthesis, token))),
            None => (None, Err(ExpectedButEOF(OpenParenthesis))),
        },
        (None, Ok(Parsed(Token::Else))) => match parse_statement(tokens) {
            (Some(else_statement), Ok(NotParsed)) => (
                Some(Statement::Else(Box::new(else_statement))),
                Ok(NotParsed),
            ),
            (None, Err(err)) => (None, Err(err)),
            _ => (
                None,
                Err(Error(
                    "Error while parsing if \"(\" <expression> \")\" ... ;",
                )),
            ),
        },
        parsed => return parsed,
    }
}

fn parse_while_statement(
    tokens: &mut impl Iterator<Item = Token>,
) -> (Option<Statement>, ParseResult) {
    match parse_if_else_statement(tokens) {
        (None, Ok(Parsed(While))) => match tokens.next() {
            Some(OpenParenthesis) => match parse_expression(tokens) {
                (Some(while_condition), Ok(Parsed(CloseParenthesis))) => {
                    match parse_statement(tokens) {
                        (Some(while_statement), Ok(NotParsed)) => (
                            Some(Statement::While(while_condition, Box::new(while_statement))),
                            Ok(NotParsed),
                        ),
                        (None, Err(err)) => (None, Err(err)),
                        _ => (
                            None,
                            Err(Error(
                                "Error while parsing while \"(\" <expression> \")\" ... ;",
                            )),
                        ),
                    }
                }
                (None, Err(err)) => (None, Err(err)),
                _ => (
                    None,
                    Err(Error("Error while parsing while \"(\" <expression> \")\"")),
                ),
            },

            Some(token) => (None, Err(Expected(OpenParenthesis, token))),
            None => (None, Err(ExpectedButEOF(OpenParenthesis))),
        },
        parsed => parsed,
    }
}

fn parse_for_statement(
    tokens: &mut impl Iterator<Item = Token>,
) -> (Option<Statement>, ParseResult) {
    match parse_while_statement(tokens) {
        (None, Ok(Parsed(For))) => match tokens.next() {
            Some(OpenParenthesis) => match parse_statement(tokens) {
                (Some(init_statement), Ok(NotParsed)) => match parse_expression(tokens) {
                    (Some(condition), Ok(Parsed(Semicolon))) => match parse_expression(tokens) {
                        (Some(increment), Ok(Parsed(CloseParenthesis))) => {
                            match parse_statement(tokens) {
                                (Some(body_statement), Ok(NotParsed)) => (
                                    Some(Statement::For(
                                        Box::new(init_statement),
                                        condition,
                                        increment,
                                        Box::new(body_statement),
                                    )),
                                    Ok(NotParsed),
                                ),
                                res => panic!("Error while parsing For : {:#?}", res),
                            }
                        }
                        res => panic!("Error while parsing For : {:#?}", res),
                    },
                    res => panic!("Error while parsing For : {:#?}", res),
                },
                res => panic!("Error while parsing For : {:#?}", res),
            },
            res => panic!("Error while parsing For : {:#?}", res),
        },
        parsed => parsed,
    }
}

fn parse_statement(tokens: &mut impl Iterator<Item = Token>) -> (Option<Statement>, ParseResult) {
    parse_for_statement(tokens)
}

/*

    <body> ::= { <statement> }
*/
fn parse_body(tokens: &mut impl Iterator<Item = Token>) -> (Option<Body>, ParseResult) {
    let mut last_statement: Option<Statement> = None;
    let mut body = Vec::new();
    loop {
        match parse_statement(tokens) {
            (Some(Statement::Else(else_statement)), Ok(NotParsed)) => match last_statement {
                Some(ref mut expr) => {
                    if let Err(..) = expr.try_add_else_statement(else_statement) {
                        println!("statement : {:#?}", expr);
                        return (
                            None,
                            Err(Error("Cannot use else without having a previous if")),
                        );
                    }
                }
                None => {
                    return (
                        None,
                        Err(Error("Cannot use else without having a previous if")),
                    )
                }
            },

            (Some(statement), Ok(NotParsed)) => {
                if let Some(tmp_statement) = last_statement.take() {
                    body.push(tmp_statement);
                }
                last_statement = Some(statement);
            }

            (None, Ok(Parsed(CloseBrace))) => {
                if let Some(tmp_statement) = last_statement.take() {
                    body.push(tmp_statement);
                }
                return (Some(Body(body)), Ok(NotParsed));
            }

            (None, Err(err)) => return (None, Err(err)),

            _ => return (None, Err(Error("Error while parsing <body> ..."))),
        }
    }
}

/*

    function ::= "int" <identifier> "(" ")" "{" <body> "}"
*/
fn parse_function_declaration(
    tokens: &mut impl Iterator<Item = Token>,
) -> (Option<Function>, ParseResult) {
    use Token::*;
    let mut parameters = Vec::new();
    match tokens.next() {
        Some(TypeToken(function_type)) => match parse_unary_factors(tokens) {
            (Some(expression), Ok(Parsed(OpenParenthesis))) => {
                let (function_name, function_type_deepness) =
                    expression.extract_variable_pointers();

                loop {
                    match tokens.next() {
                        Some(TypeToken(Void(0))) => break,
                        Some(TypeToken(expression_type)) => match parse_unary_factors(tokens) {
                            (Some(expression), Ok(Parsed(op @ Comma)))
                            | (Some(expression), Ok(Parsed(op @ CloseParenthesis))) => {
                                let (name, deepness) = expression.extract_variable_pointers();
                                parameters.push(Variable(
                                    name,
                                    expression_type.modify_deepness(deepness as isize),
                                ));
                                if op == CloseParenthesis {
                                    break;
                                }
                            }

                            (None, Err(err)) => return (None, Err(err)),
                            _ => {
                                return (
                                    None,
                                    Err(Error("Error while parsing function parameters")),
                                )
                            }
                        },

                        Some(CloseParenthesis) => break,

                        Some(token) => return (None, Err(Expected(TypeToken(Unknown), token))),
                        None => return (None, Err(ExpectedButEOF(TypeToken(Unknown)))),
                    }
                }
                match parse_statement(tokens) {
                    (Some(Statement::BodyStatement(mut body)), Ok(NotParsed)) => {
                        body.0.push(Statement::Return(ConstantInt(0)));
                        (
                            Some(Function(
                                function_type.modify_deepness(function_type_deepness as isize),
                                function_name,
                                parameters,
                                body,
                            )),
                            Ok(NotParsed),
                        )
                    }

                    (_, result) => (None, result),
                }
            }

            _ => {
                return (
                    None,
                    Err(Error("Error while parsing function type / function name")),
                )
            }
        },
        _ => (None, Err(Error("Invalid function declaration"))),
    }
}

/*

    program ::= <function>
*/
pub fn parse_program(tokens: &mut impl Iterator<Item = Token>) -> Result<Program, ParseError> {
    match parse_function_declaration(tokens) {
        (Some(function), _) => Ok(Program(vec![function])),
        (_, Err(result)) => Err(result),
        wtf => panic!("WTF : {:#?}", wtf),
    }
}
