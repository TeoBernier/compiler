
use compiler::utils::{lex_file, write_assembly_file, compile_assembly_file, test_gcc};
use compiler::token::token_type::Token;
use compiler::tree::parse::parse_program;
use std::env;

fn main() {
    for path in env::args().skip(1) {

        println!("Lexing {}", path);
        println!("{:?}", lex_file(&path).collect::<Vec<Token>>());

        let mut tokens = lex_file(&path);
    
        let program = parse_program(&mut tokens);
        let program = match program {
            Ok(program) => program,
            Err(erreur) => panic!("Erreur : {:#?}\nParser : {:#?}", erreur, tokens),
        };
        println!("{:#?}", program);

        let assembly_path = path.strip_suffix("c").unwrap().to_string() + "s";
    

        test_gcc(&path);
        write_assembly_file(&assembly_path, program);
        compile_assembly_file(&assembly_path);
    }
}