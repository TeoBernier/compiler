use crate::token::token_type::*;
use crate::utils::CharsIterator;
use Token::*;

use crate::tree::parse::TYPES;

fn extract_identifier_literal(
    chars: &mut CharsIterator,
    c: Option<char>,
) -> Result<(Token, Option<char>), ()> {
    let mut s = match c {
        Some(c1) => c1.to_string(),
        None => String::new(),
    };
    loop {
        match chars.next() {
            Some(c @ 'a'..='z') | Some(c @ 'A'..='Z') | Some(c @ '0'..='9') | Some(c @ '_') => {
                s.push(c)
            }
            c => {
                let ch = match c {
                    Some('\0'..=' ') => None,
                    _ => c,
                };
                return Ok((
                    if let Some(keywordtoken) = TOKEN_STRING.get(s.as_str()) {
                        keywordtoken.clone()
                    } else if let Some(&declaration_type) = TYPES.lock().unwrap().get(s.as_str()) {
                        TypeToken(declaration_type)
                    } else {
                        Identifier(Box::leak(s.into_boxed_str()))
                    },
                    ch,
                ));
            }
        }
    }
}

fn extract_decimal_literal(
    chars: &mut CharsIterator,
    c: Option<char>,
) -> Result<(Token, Option<char>), ()> {
    let mut value: u64;
    match c {
        Some(c @ '0'..='9') => value = c as u64 - '0' as u64,
        None => value = 0,
        _ => return Err(()),
    };

    loop {
        match chars.next() {
            Some(c @ '0'..='9') => value = value * 10 + c as u64 - '0' as u64,
            Some('\0'..=' ') => return Ok((IntValue(value), None)),
            c => return Ok((IntValue(value), c)),
        }
    }
}

fn extract_hexadecimal_literal(
    chars: &mut CharsIterator,
    c: Option<char>,
) -> Result<(Token, Option<char>), ()> {
    let mut value: u64;
    match c {
        Some(c @ 'a'..='f') => value = c as u64 - 'a' as u64 + 10,
        Some(c @ 'A'..='F') => value = c as u64 - 'A' as u64 + 10,
        Some(c @ '0'..='9') => value = c as u64 - '0' as u64,
        None => value = 0,
        _ => return Err(()),
    };

    loop {
        match chars.next() {
            Some(c @ 'a'..='f') => value = value * 16 + c as u64 - 'a' as u64 + 10,
            Some(c @ 'A'..='F') => value = value * 16 + c as u64 - 'A' as u64 + 10,
            Some(c @ '0'..='9') => value = value * 16 + c as u64 - '0' as u64,
            Some('\0'..=' ') => return Ok((IntValue(value), None)),
            c => return Ok((IntValue(value), c)),
        }
    }
}

fn extract_octal_literal(
    chars: &mut CharsIterator,
    c: Option<char>,
) -> Result<(Token, Option<char>), ()> {
    let mut value: u64;
    match c {
        Some(c @ '0'..='7') => value = c as u64 - '0' as u64,
        None => value = 0,
        _ => return Err(()),
    };

    loop {
        match chars.next() {
            Some(c @ '0'..='7') => value = value * 8 + c as u64 - '0' as u64,
            Some('\0'..=' ') => return Ok((IntValue(value), None)),
            c => return Ok((IntValue(value), c)),
        }
    }
}

fn extract_binary_literal(
    chars: &mut CharsIterator,
    c: Option<char>,
) -> Result<(Token, Option<char>), ()> {
    let mut value: u64;
    match c {
        Some(c @ '0'..='1') => value = c as u64 - '0' as u64,
        None => value = 0,
        _ => return Err(()),
    };

    loop {
        match chars.next() {
            Some(c @ '0'..='1') => value = value * 2 + c as u64 - '0' as u64,
            Some('\0'..=' ') => return Ok((IntValue(value), None)),
            c => return Ok((IntValue(value), c)),
        }
    }
}

fn extract_number_literal(
    chars: &mut CharsIterator,
    c: Option<char>,
) -> Result<(Token, Option<char>), ()> {
    match c {
        Some('0') => match chars.next() {
            Some('x') => extract_hexadecimal_literal(chars, None),
            Some('o') => extract_octal_literal(chars, None),
            Some('b') => extract_binary_literal(chars, None),
            c @ Some('0'..='9') => extract_decimal_literal(chars, c),
            Some('\0'..=' ') => Ok((IntValue(0), None)),
            c => Ok((IntValue(0), c)),
        },
        c @ Some('1'..='9') => extract_decimal_literal(chars, c),
        None => {
            let tmp = chars.next();
            extract_number_literal(chars, tmp)
        }
        _ => Err(()),
    }
}

fn extract_special_character(
    chars: &mut CharsIterator,
    token: Option<Token>,
    c: char,
) -> Result<(Token, Option<char>), ()> {
    let value = match token {
        Some(token) => match (token, c) {
            (BitwiseShiftRight, '=') => Ok((BitwiseShiftRightAssignment, None)),
            (BitwiseShiftLeft, '=') => Ok((BitwiseShiftLeftAssignment, None)),
            (LogicalNegation, '=') => Ok((NotEqual, None)),
            (Multiplication, '=') => Ok((MultiplicationAssignment, None)),
            (BitwiseXor, '=') => Ok((BitwiseXorAssignment, None)),
            (BitwiseAnd, '=') => Ok((BitwiseAndAssignment, None)),
            (Negation, '=') => Ok((SubtractionAssignment, None)),
            (BitwiseOr, '=') => Ok((BitwiseOrAssignment, None)),
            (Remainder, '=') => Ok((RemainderAssignment, None)),
            (Division, '=') => Ok((DivisionAssignment, None)),
            (Addition, '=') => Ok((AdditionAssignment, None)),
            (Negation, '-') => Ok((Decrement, None)),
            (Addition, '+') => Ok((Increment, None)),

            (Greater, '>') => Ok((BitwiseShiftRight, None)),
            (Less, '<') => Ok((BitwiseShiftLeft, None)),
            (Assignment, '=') => Ok((Equal, None)),
            (BitwiseAnd, '&') => Ok((And, None)),
            (BitwiseOr, '|') => Ok((Or, None)),
            (Greater, '=') => Ok((GreaterEqual, None)),
            (Less, '=') => Ok((LessEqual, None)),
            (Division, '/') => {
                chars.skip_until('\n');
                return Ok((Comment, None));
            }

            (Division, '*') => {
                loop {
                    chars.skip_until('*');
                    if let Some('/') = chars.next_char() {
                        break;
                    }
                }
                return Ok((Comment, None));
            }

            (token, c) => Ok((token, Some(c))),
        },
        None => match c {
            '~' => Ok((BitwiseComplement, None)),
            ')' => Ok((CloseParenthesis, None)),
            '(' => Ok((OpenParenthesis, None)),
            '!' => Ok((LogicalNegation, None)),
            '*' => Ok((Multiplication, None)),
            '=' => Ok((Assignment, None)),
            '&' => Ok((BitwiseAnd, None)),
            '^' => Ok((BitwiseXor, None)),
            '}' => Ok((CloseBrace, None)),
            '{' => Ok((OpenBrace, None)),
            ';' => Ok((Semicolon, None)),
            '%' => Ok((Remainder, None)),
            '|' => Ok((BitwiseOr, None)),
            '-' => Ok((Negation, None)),
            '+' => Ok((Addition, None)),
            '/' => Ok((Division, None)),
            '>' => Ok((Greater, None)),
            '<' => Ok((Less, None)),
            ',' => Ok((Comma, None)),
            '_' => return extract_identifier_literal(chars, Some('_')),
            _ => Err(()),
        },
    };
    match value {
        Ok((token, None)) => match chars.next() {
            Some(c) => return extract_special_character(chars, Some(token), c),
            None => return Ok((token, None)),
        },
        _ => return value,
    }
}

pub fn first_token(
    chars: &mut CharsIterator,
    mut c: Option<char>,
) -> Option<(Token, Option<char>)> {
    if let None = c {
        c = chars.next();
    };
    loop {
        match c {
            None => return None,
            Some(c1) => {
                match c1 {
                    //One char token
                    '!'..='/' | ':'..='@' | '['..='`' | '{'..='~' => {
                        return match extract_special_character(chars, None, c1) {
                            Ok((Comment, c)) => first_token(chars, c),
                            Ok((token, c)) => Some((token, c)),
                            Err(()) => panic!("Error while parsing tokens !"),
                        }
                    }
                    'A'..='Z' | 'a'..='z' => {
                        return Some(extract_identifier_literal(chars, Some(c1)).unwrap())
                    }
                    '0'..='9' => return Some(extract_number_literal(chars, Some(c1)).unwrap()),
                    _ => c = chars.next(),
                }
            }
        }
    }
}

use std::fmt::Debug;

#[derive(Debug)]
struct TokenIterator {
    iterator: CharsIterator,
    c: Option<char>,
}

impl Iterator for TokenIterator {
    type Item = Token;
    fn next(&mut self) -> Option<Self::Item> {
        match first_token(&mut self.iterator, self.c) {
            Some((token, c)) => {
                self.c = c;
                Some(token)
            }
            None => None,
        }
    }
}

pub fn tokenize(chars: CharsIterator) -> impl Iterator<Item = Token> + Debug {
    TokenIterator {
        iterator: chars,
        c: None,
    }
}
