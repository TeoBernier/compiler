use crate::tree::objects::expression_type::ExpressionType;

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Token {
    //syntax / program structure
    OpenBrace,
    CloseBrace,
    OpenParenthesis,
    CloseParenthesis,
    Semicolon,
    Comma,

    // // /* */
    Comment,

    //Type
    TypeToken(ExpressionType),

    //Literal
    Identifier(&'static str),

    //Number
    IntValue(u64),
    FloatValue(f64),

    //keyword
    Return,
    If,
    Else,
    While,
    For,

    //unary operation
    Negation,
    BitwiseComplement,
    LogicalNegation,
    Increment,
    Decrement,

    //logical operation
    And,
    Or,
    Equal,
    NotEqual,
    Less,
    LessEqual,
    Greater,
    GreaterEqual,

    //binary operation
    Addition,
    Multiplication,
    Division,
    Remainder,
    BitwiseAnd,
    BitwiseOr,
    BitwiseXor,
    BitwiseShiftLeft,
    BitwiseShiftRight,

    //assignment operation
    Assignment,
    AdditionAssignment,
    SubtractionAssignment,
    MultiplicationAssignment,
    DivisionAssignment,
    RemainderAssignment,
    BitwiseAndAssignment,
    BitwiseOrAssignment,
    BitwiseXorAssignment,
    BitwiseShiftLeftAssignment,
    BitwiseShiftRightAssignment,
}

pub fn assignment_to_binary_operation(token: &Token) -> Token {
    match token {
        AdditionAssignment => Addition,
        SubtractionAssignment => Negation,
        MultiplicationAssignment => Multiplication,
        DivisionAssignment => Division,
        RemainderAssignment => Remainder,
        BitwiseAndAssignment => BitwiseAnd,
        BitwiseOrAssignment => BitwiseOr,
        BitwiseXorAssignment => BitwiseXor,
        BitwiseShiftLeftAssignment => BitwiseShiftLeft,
        BitwiseShiftRightAssignment => BitwiseShiftRight,
        _ => unreachable!(),
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum ParseOk {
    Parsed(Token),
    NotParsed,
}

#[derive(Debug, Clone, Copy)]
pub enum ParseError {
    EOF,
    ExpectedButEOF(Token),
    Expected(Token, Token),
    Error(&'static str),
}

pub type ParseResult = Result<ParseOk, ParseError>;

use lazy_static::lazy_static;
use std::collections::HashMap;
use Token::*;
lazy_static! {
    pub static ref TOKEN_STRING: HashMap<&'static str, Token> = {
        let mut m = HashMap::new();
        m.insert("return", Return);
        m.insert("if", If);
        m.insert("else", Else);
        m.insert("while", While);
        m.insert("for", For);
        m
    };
}

pub static SYNTAX: [Token; 5] = [
    OpenBrace,
    CloseBrace,
    OpenParenthesis,
    CloseParenthesis,
    Semicolon,
];

pub static KEYWORDS: [Token; 5] = [Return, If, Else, While, For];

pub static CONDITIONNAL_OPERATION: [Token; 8] = [
    And,
    Or,
    Equal,
    NotEqual,
    Less,
    LessEqual,
    Greater,
    GreaterEqual,
];

pub static UNARY_OPERATION: [Token; 5] = [
    Negation,
    BitwiseComplement,
    LogicalNegation,
    Increment,
    Decrement,
];

pub static BINARY_OPERATION: [Token; 9] = [
    Addition,
    Multiplication,
    Division,
    Remainder,
    BitwiseAnd,
    BitwiseOr,
    BitwiseXor,
    BitwiseShiftLeft,
    BitwiseShiftRight,
];

pub static ASSIGNMENT_OPERATION: [Token; 11] = [
    Assignment,
    AdditionAssignment,
    SubtractionAssignment,
    MultiplicationAssignment,
    DivisionAssignment,
    RemainderAssignment,
    BitwiseAndAssignment,
    BitwiseOrAssignment,
    BitwiseXorAssignment,
    BitwiseShiftLeftAssignment,
    BitwiseShiftRightAssignment,
];
