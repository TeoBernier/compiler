use std::convert::TryFrom;
use std::io::{BufReader, Bytes, Read, Write};
use std::iter::Map;

const CONT_MASK: u8 = 0b0011_1111;

#[inline]
fn utf8_first_byte(byte: u8, width: u32) -> u32 {
    (byte & (0x7F >> width)) as u32
}

/// Returns the value of `ch` updated with continuation byte `byte`.
#[inline]
fn utf8_acc_cont_byte(ch: u32, byte: u8) -> u32 {
    (ch << 6) | (byte & CONT_MASK) as u32
}

#[inline]
fn unwrap_or_0(opt: Option<u8>) -> u8 {
    match opt {
        Some(byte) => byte,
        None => 0,
    }
}

/// Reads the next code point out of a byte iterator (assuming a
/// UTF-8-like encoding).
#[inline]
pub fn next_code_point<'a, I: Iterator<Item = u8>>(bytes: &mut I) -> Option<u32> {
    // Decode UTF-8
    let x = bytes.next()?;
    if x < 128 {
        return Some(x as u32);
    }

    // Multibyte case follows
    // Decode from a byte combination out of: [[[x y] z] w]
    // NOTE: Performance is sensitive to the exact formulation here
    let init = utf8_first_byte(x, 2);
    let y = unwrap_or_0(bytes.next());
    let mut ch = utf8_acc_cont_byte(init, y);
    if x >= 0xE0 {
        // [[x y z] w] case
        // 5th bit in 0xE0 .. 0xEF is always clear, so `init` is still valid
        let z = unwrap_or_0(bytes.next());
        let y_z = utf8_acc_cont_byte((y & CONT_MASK) as u32, z);
        ch = init << 12 | y_z;
        if x >= 0xF0 {
            // [x y z w] case
            // use only the lower 3 bits of `init`
            let w = unwrap_or_0(bytes.next());
            ch = (init & 7) << 18 | utf8_acc_cont_byte(y_z, w);
        }
    }

    Some(ch)
}

use std::io::Result;
#[derive(Debug)]
pub struct CharsIterator {
    iterator: Map<Bytes<BufReader<File>>, Box<dyn FnMut(Result<u8>) -> u8>>,
    line_offset: usize,
    char_offset: usize,
}

impl CharsIterator {
    pub fn new(path: &str) -> Self {
        Self {
            iterator: BufReader::new(File::open(path).expect("Cannot open file"))
                .bytes()
                .map(Box::new(|result| result.expect("Problem reading file"))),
            line_offset: 0,
            char_offset: 0,
        }
    }

    fn iter_inner(&mut self) -> Option<char> {
        match next_code_point(&mut self.iterator)
            .map(|bytes| TryFrom::<u32>::try_from(bytes).expect("Cannot read file"))
        {
            Some('\n') => {
                self.line_offset += 1;
                self.char_offset = 0;
                Some('\n')
            }
            some @ Some(..) => {
                self.char_offset += 1;
                some
            }
            None => None,
        }
    }

    pub fn skip_until(&mut self, ch: char) {
        loop {
            match self.iter_inner() {
                Some(b) if b == ch => break,
                None => break,
                _ => (),
            }
        }
    }

    pub fn next_char(&mut self) -> Option<char> {
        self.iter_inner()
    }
}

impl Iterator for CharsIterator {
    type Item = char;

    fn next(&mut self) -> Option<Self::Item> {
        match self.iter_inner() {
            Some('\n') => self.next(),
            res => res,
        }
    }
}

pub fn read_chars(path: &str) -> CharsIterator {
    CharsIterator::new(path)
}

use super::token::{extracters::tokenize, token_type::Token};
use std::fmt::Debug;
use std::fs::File;

pub fn lex_file(path: &str) -> impl Iterator<Item = Token> + Debug {
    tokenize(read_chars(path))
}

use super::tree::{generate::generate_program, objects::program::Program};
pub fn write_assembly_file(path: &str, program: Program) {
    File::create(path)
        .expect("Cannot create file")
        .write(generate_program(&program).as_bytes())
        .expect("Cannot write file");
}

use std::process::Command;
pub fn compile_assembly_file(path: &str) {
    let path_exec = &(path.strip_suffix("s").unwrap().to_string() + "exe");

    println!(
        "\nUsing custom compiler :\n\nCompilation : {:?}",
        Command::new("gcc")
            .args(&["-g", "-Wall", "-o", path_exec, path])
            .output()
            .expect("failed to compile program")
    );
    execute_file(path_exec);
}

pub fn test_gcc(path: &str) {
    let path_assembly = &(path.strip_suffix(".c").unwrap().to_string() + "_gcc.s");
    let path_exec = &(path.strip_suffix(".c").unwrap().to_string() + "_gcc.exe");

    println!(
        "\nUsing gcc :\n\nCompilation : {:?}",
        Command::new("gcc")
            .args(&["-g", "-Wall", "-O3", "-S", "-o", path_assembly, path])
            .output()
            .expect("failed to compile program")
    );

    println!(
        "              {:?}",
        Command::new("gcc")
            .args(&["-g", "-Wall", "-o", path_exec, path_assembly])
            .output()
            .expect("failed to compile program")
    );
    execute_file(path_exec);
}

pub fn execute_file(path: &str) {
    println!(
        "\nExecution   : {:?}\n",
        Command::new("./".to_string() + path)
            .output()
            .expect("failed to execute program")
    );
}
